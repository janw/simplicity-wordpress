<?php
/**
 * @package WordPress
 * @subpackage Simplicity
 * Statisches Seiten-Template. Minimalisiert ohne Meta, Kommentare, Navi
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
<a href="<?php the_permalink() ?>">
<div id="pages">

	<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
		<h1 class="storytitle"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
				
		<div class="storycontent">
			<?php the_content(__('(more...)')); ?>
		</div>
		
		<p class="storydate alignright"><?php edit_post_link(__('&nbsp;&#8984; Bearbeiten&nbsp;')); ?></p>
	</div>

</div>
	
<?php endwhile; ?>

<?php get_footer(); ?>

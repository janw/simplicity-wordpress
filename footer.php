<?php
/**
 * @package WordPress
 * @subpackage Simplicity
 */
?>
</div>

<!-- begin footer -->
<div id="footer">
			


<div id="menufooter">
	<ul>
		<?php if ( !function_exists('dynamic_sidebar') ||
		           !dynamic_sidebar('Footer bar small') ) : ?>
		<?php endif; ?>
	</ul>
</div>

<p class="credit">God bless <a href='http://wordpress.org/'><strong>WordPress</strong></a>.</p>


<?php wp_footer(); ?>

</div>
</div>

</body>
</html>
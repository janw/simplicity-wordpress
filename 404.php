<?php
/**
 * @package WordPress
 * @subpackage Simplcitiy
 * Klassisches 404-Doc. Condensed to the most essential.
 */
get_header();
?>
	<div class="nofeedback"></div>

<div id="pages">

		<h1 class="nodate">Uuups!</h1>
			<p class="columns">Wie es aussieht existiert die von dir aufgerufene Seite nicht. Falls du diesen Link von mir oder jemand anderem genau so bekommen hast, benachrichtige mich gerne dar&uuml;ber. Bitte kehre zur Startseite zur&uuml;ck, um von dort aus mein Blog weiter zu durchst&ouml;bern.
			</p>
		 <p class="moretext">
			<a href="<?php bloginfo('url'); ?>/">Zur Startseite zur&uuml;ckkehren &raquo;</a>
		</p>
		<hr>

</div>

<?php get_footer(); ?>

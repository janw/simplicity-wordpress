# SIMPLICITY Wordpress Theme

## Synposis
**Simplicity is a WordPress theme designed and build by [Janwillhaus](http://janwillhaus.de). Is is based on its predecessor, the *Awesomeness* theme, which is not available to public, yet. It was also build by Janwillhaus, and delivered most of Simplicity's DNA. Its soul is still incorporated into Simplicity and now delivers this beautiful theme.**

## Features
* Simplicity is compatible with WordPress version 3.3 and above
* The sidebar is fixed in position and should not be used with more than three widgets. To keep the sidebar inside the screen.
* The sidebar's logo is not a widget. It is part of the design and can be changed (see [instructions](#installation) below).
* It is build with a nice responsive component, which delivers compatibility for almost every screen size and also mobile devices.

## Installation
The installation of Simplicity is pretty easy. Simply place the complete folder inside of your WordPress's wp-content/themes/. If you want to change the logo in the sidebar (or above, in responsive one column mode), simply replace `simplicity/images/logo.png` with your own file. It should be square size of 160px, with no need of rounded corners. Those are CSS3, dude. ;)

For Apple's iDevices, this theme features precomposed homescreen icons. They are integrated in the `<head>`-Tag and the files are placed in `simplicity/images/iconXXX.png`, while `XXX` represents some different suffixes. Further information about these icons [can be found at Apple](http://developer.apple.com/library/ios/#DOCUMENTATION/UserExperience/Conceptual/MobileHIG/IconsImages/IconsImages.html#//apple_ref/doc/uid/TP40006556-CH14).

## Licensing

**SIMPLICITY** is licensed under *Creative Commons BY-NC-SA 3.0*.

**In short:** you are free to share and make derivative works of the files under the conditions that you do not use it for commercial purposes, appropriately attribute it, and that you distribute it only under a license identical to this one. The full license can be found here: [http://creativecommons.org/licenses/by-nc-sa/3.0/](http://creativecommons.org/licenses/by-nc-sa/3.0/)

**Disclaimer:** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
<?php
/*
 * @package WordPress
 * @subpackage Simplicity
 */
get_header();
?>


<!-- Single Article Styling -->
<style type="text/css" media="screen">
	.first-stanza{font-style: italic;}
</style>
<!-- /Single Article Styling -->


<?php while (have_posts()) : the_post(); ?>

<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
	<h1 class="storytitle"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>

	<p class="storydate"><?php the_time('j. F Y') ?> &nbsp;&nbsp;<?php edit_post_link(__('&#8984; Bearbeiten')); ?></p>

	<div class="storycontent">
		<?php if (has_post_thumbnail()) { ?>
        	<?php the_post_thumbnail('thumbnail'); ?>
    	<?php } ?> <?php the_content('<span class="moretext">&raquo;Weiterlesen &raquo;</div>'); ?>
	</div>
	
	
	<div id="nav">
		
		<div class="alignleft">
			<?php previous_post_link('%link', '&laquo; Vorheriger Artikel <p class="title">%title</p>', FALSE); ?>
		</div>
		
		<div class="alignright textalignright">
			<?php next_post_link('%link', 'N&auml;chster Artikel &raquo; <p class="title">%title</p>', FALSE); ?>
		</div>
		
		<hr>

	</div>
	

</div>

<?php comments_template(); // Get wp-comments.php template ?>

<?php endwhile; ?>


<?php get_footer(); ?>
